import requests
import pandas as pd
from time import sleep
from tqdm.auto import tqdm
#Reading the CSV file 
df = pd.read_csv("This is the file directory for each batch that was used")
#Getting the tokens from github
tokenList=["List of Tokens"]
token_index = 0
headers= {"Authorization": "token " + tokenList[token_index]}
index = 0

#Inserting this for checking what PRs have already been checked
first_unchecked_index = df[df['checked'].isnull()].index[0]
#in some cases, pr_api_url is missing, and is in pr_url, this is for that
df['pr_api_url'].fillna(df['pr_url'], inplace=True)

#itterating through the CSV file and pull requests to get the info needed
for index, url in tqdm(enumerate(df.pr_api_url)):
    if index < first_unchecked_index:
        continue
#Sending the request to GitHub API    
    try:
        response = requests.get(url, headers=headers)
        rate_limit_remaining = int(response.headers["X-RateLimit-Remaining"])
    except Exception as e:
        print(f"An error occurred while sending a request to {url}: {e}")
        pass
    while rate_limit_remaining < 50:
        token_index = (token_index + 1)
        if token_index >= len(tokenList):
            token_index = 0        
        headers = {"Authorization": "token " + tokenList[token_index]}   
        try:
            response = requests.get(url, headers=headers)
            rate_limit_remaining = int(response.headers["X-RateLimit-Remaining"])
        except KeyError:
            print("The 'X-RateLimit-Remaining' header is not present in the response.")
            pass
        sleep(20)
    
    if response.status_code == 200:   #if this pull requests exists and is not deleted
        pull_request = response.json()
        author_id = pull_request["user"]["login"]
        closer_id = pull_request["merged_by"]["login"] if pull_request["merged_by"] else None
        author_pr_comment = pull_request["body"]
        comments_url = pull_request["comments_url"]
        try:
            comments_response = requests.get(comments_url, headers=headers)
            if comments_response.status_code == 200:
                comments_data = comments_response.json()
                comments_body = [comment["body"] for comment in comments_data]
            else:
                print("Failed to retrieve comments for pull request:", comments_response.json()["message"])
                comments_body = None
        except Exception as e:
            print(f"An error occurred while sending a request to {url}: {e}")
            continue   
        commits_count = pull_request["commits"]
        code_changes = pull_request["changed_files"]
        creation_date = pull_request["created_at"]
        closing_date = pull_request["closed_at"]
        comments_counts = pull_request["comments"]
        completed = 0 if pd.isna(df.at[index, "checked"]) else "1"
        
        # Add the new data to the existing data
        df.at[index, "author_id"] = author_id
        df.at[index, "author_desc_body"] = author_pr_comment
        df.at[index, "closer_id"] = closer_id
        df.at[index, "comments_counts"] = comments_counts
        df.at[index, "comments"] = comments_body
        df.at[index, "commit_counts"] = commits_count
        df.at[index, "code_changes_counts"] = code_changes
        df.at[index, "created_at"] = creation_date
        df.at[index, "closed_at"] = closing_date
        df.at[index, "checked"] = completed
        
        if index % 500 == 0:
            df.to_csv("Output file", index=False)   #save on every 500 fetched prs
        
        
    else:  #if the pr is deleted

        df.at[index, "checked"] = "1"
        df.at[index, "author_id"] = "Deleted"
        df.at[index, "author_desc_body"] = "Deleted"
        df.at[index, "closer_id"] = "Deleted"
        df.at[index, "comments_counts"] = "Deleted"
        df.at[index, "comments"] = "Deleted"
        df.at[index, "commit_counts"] = "Deleted"
        df.at[index, "code_changes_counts"] = "Deleted"
        df.at[index, "created_at"] = "Deleted"
        df.at[index, "closed_at"] = "Deleted"
df.to_csv("Output file", index=False)
