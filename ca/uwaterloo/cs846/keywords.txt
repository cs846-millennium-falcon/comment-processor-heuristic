 : no-reason
multiple change, too many, lots of change, many change, many commit, unfinished, mess, unclear : chaotic
already, check, fixed, similar, fulfillment, reference, contain, duplicate, another, applied, implemented, resolved in, included, updated, refer, same problem as, retargeted, dupe : duplicate
chaotic, multiple change, too many, lots of change, many change, many commit, unfinished, mess, unclear : chaotic
already done, check, fixed, similar, fulfillment, reference, contain, duplicate, another, applied, implemented, resolved in, included, updated, refer, same problem as, retargeted, dupe : duplicate
merge conflict, testcase, test, auto-merge, incompatible, competing, conflict, build errors, build fail, manual, not allow, another commit : merge-conflict
suggestion, proper pr, issue : not-pr
fix, reopen, reliable, griefing, false, break, backward, unrelated, rework, verify, add, invalid, error, quality of change, back out, problem, code style, redundant, disagree, improve, incomplete, incorrect, not suit, negative, standard, revert necessary, missing, changes need, need change, fragile, null : low-quality
successful, fixed, completed,committed, merged,rebased : successful
no reply, inactive, inactivity, old, long time, outdated, out of date, abandoned, no continuation, lack of momentum, delay, stale, no longer, lack of activity, reacted, closing : stale
going to close,no benefit, preference, prefer, branch, repo, version, by mistake, out of date, unnecessary, contradict, disregarded, as is, not needed, not useful, benefit, no longer need, supersede, overrule  : unnecessary
fixed-in,re-submit, resubmit, another, replaced, better, more, continue in, supersede, in favour of, in favor of, new pr, move, updated : replaced
