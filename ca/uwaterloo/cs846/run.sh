#!/usr/bin/env bash

if [ $# -ne 2 ]; then
  echo "Usage: $0 keyword_file pr_comment_file"
  exit 1
fi

echo "Starting PR comments file processing"
keyword_f=$1
input_f=$2
./HeuristicClassifier.py $keyword_f $input_f

 exit 0