#!/usr/bin/env python3
#
# This is code use to generate statistics and histograms on processed comments
#
# @author Xiaoyu Zhou, xiaoyu.zhou@uwaterloo.ca, M.Math, Faculty of Math

import sys
import time
import datetime
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

def show_hist(input_file):
    form = pd.read_csv(input_file, usecols={0, 4})
    
    # Get count num for each category.
    counts = form.groupby(['comment_group']).count()
    counts.columns = ['counts']
    print(counts)

    # Show the histogram for the data, and sav to './histogram.png'.
    plt.figure(figsize=(20, 10))
    sns.histplot(form['comment_group'].sort_values(), shrink=0.8)
    plt.xticks(rotation=30)
    plt.xlabel('PRs Category')
    plt.ylabel('Number of PR')
    plt.title('The Number of PR for Different Categories')
    # plt.show()
    plt.show()
    plt.savefig('./histogram.png')

    # Show the histogram of the percentage for the data, and sav to './histogram_percent.png'.
    plt.figure(figsize=(20, 10))
    sns.histplot(form['comment_group'].sort_values(), shrink=0.8, stat='probability')
    plt.xticks(rotation=30)
    plt.xlabel('PRs Category')
    plt.ylabel('Percentage of Each Category')
    plt.title('The Percentage of Each Category')
    # plt.show()
    plt.savefig('./histogram_percent.png')
    

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: " + sys.argv[0] + " format failed")
        print("[python statistics.py pull_request_out.csv]")
        exit(-2)

    # Grab current time before running the code
    start = time.time()

    pr_out_file = sys.argv[1]
    show_hist(pr_out_file)
    
    # Grab current time after running the code
    end = time.time()
    total_time = end - start
    print("\n Execution Time: " + str(datetime.timedelta(seconds=total_time)))