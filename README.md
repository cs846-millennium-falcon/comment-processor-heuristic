# comment-processor-heuristic
This is a program for parsing pull requests comments via a heuristic. It requires two inputs:
a keyword mapping file in the format word1, word2,...wordn : comment-label-group and a pull 
requests comments file in CSV format.

The program can be executed in 2 ways: either via a **./run.sh keyword.txt pull_requests.csv** command or
a **./HeuristicClassifier.py keyword.txt pull_requests.csv**. Either commands makes the 
assumption that we have a Python3 available in the path.

The program parses the comment field of the input file and tries to map it to the
correct comment labelling group.

The output of this program is a parsed result file containing the comment label grouping
of the comment. The name of the output file is **pull_request_out.csv**. It is generated
in the same directory where the code is run.

## Getting started
## Description
This program helps analyze why some open source projects are rejected by parsing
and categorizing the comments made on these rejected code submissions.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage


## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
The program was written as part of CS846: Topics in Empirical Software Evolution - Masters of Computer Science, Faculty of Math 
by the team of Ayinde, Amirreza and Xiaoyu.


## Authors and acknowledgment
* Ayinde Yakubu, ayinde.yakubu@uwaterloo.ca, HeuristicClassifier designer and developer
* Amirreza Shamsolhodaei, amirreza.shamsolhodaei@uwaterloo.ca, responsible for code to pull data from github and other repos
* Xiaoyu Zhou, xiaoyu.zhou@uwaterloo.ca, creator of keywords.txt, statistics and histogram

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
